from django.shortcuts import render, redirect, reverse
from django.db import connection
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from .forms import CreateTindakanForm, UpdateTindakanForm
from .forms import CreateTindakanPoliForm, UpdateTindakanPoliForm
import datetime
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.urls import reverse

### PROFIL PENGGUNA ###
# belom bener
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def profil(request):
    if 'id' not in request.session:
        return redirect(reverse('auth:login'))
    
    user = request.session['user']
    username = request.session['username']
    id = request.session['id']

    cursor_p = connection.cursor()

    select = "SELECT * FROM MEDIKAGO.PENGGUNA WHERE username='"+username+"';"
    cursor_p.execute(select)
    data = namedtuplefetchall(cursor_p)
    general_data = data[0]

    if user =='administrator':
        select="SELECT * FROM MEDIKAGO.ADMINISTRATOR WHERE username='"+username+"';"
        cursor_p.execute(select)
        data  = namedtuplefetchall(cursor_p)
        data_admin = data[0]
        return render(request,'profil_admin.html',{'general_data':general_data,'data_admin':data_admin})

    elif user == 'dokter':
        select="SELECT * FROM MEDIKAGO.DOKTER WHERE username='"+username+"';"
        cursor_p.execute(select)
        data = namedtuplefetchall(cursor_p)
        data_dokter = data[0]
        return render(request,'profil_dokter.html',{'general_data':general_data,'data_dokter':data_dokter})

    elif user == 'pasien':
        select="SELECT * FROM MEDIKAGO.PASIEN WHERE username='"+username+"';"
        cursor_p.execute(select)
        data  = namedtuplefetchall(cursor_p)
        data_pasien = data[0]

        no_rekam_medis = data_pasien.no_rekam_medis
        select = "SELECT * FROM MEDIKAGO.ALERGI_PASIEN WHERE no_rekam_medis='"+no_rekam_medis+"';"
        cursor_p.execute(select)
        data = namedtuplefetchall(cursor_p)
        alergi_pasien = data[0][1]
      
        return render(request,'profil_pasien.html',{'general_data':general_data,'data_pasien':data_pasien,'alergi_pasien':alergi_pasien})

### TINDAKAN POLIKLINIK ###
def create_tindakan_poli(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        create_tindakan_poli = CreateTindakanPoliForm()
        response = {'create_form': create_tindakan_poli}
        return render(request, 'create_tindakan_poli.html', response)
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        cursor.execute("SELECT id_tindakan_poli FROM medikago.tindakan_poli ORDER BY id_tindakan_poli DESC LIMIT 1")

        id_tindakan_poli = cursor.fetchone()[0].split("-")
        id_tindakan_poli[-1] = str(int(id_tindakan_poli[-1]) + 1)
        id_tindakan_poli = "-".join(id_tindakan_poli)
        id_poliklinik = request.POST["id_poliklinik"]
        nama_tindakan = request.POST["nama_tindakan"]
        deskripsi = request.POST["deskripsi"]
        tarif = request.POST["tarif"]

        try:
            cursor.execute(f"INSERT INTO medikago.tindakan_poli (id_tindakan_poli, id_poliklinik, nama_tindakan, deskripsi, tarif) VALUES ('{id_tindakan_poli}', '{id_poliklinik}', '{nama_tindakan}', '{deskripsi}', {tarif})")
        except:
            messages.error(request, "Create Tindakan Poliklinik Gagal Dilakukan")
            return redirect(reverse("app2:create_tindakan_poli"))
        return redirect(reverse("app2:view_daftar_tindakan_poli"))
    return redirect(reverse("login:pengguna"))

# udah
def view_daftar_tindakan_poli(request):
    response = {}
    data_tindakan_poli = []
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["user"] != "nothing"):
        cursor.execute(f"SELECT * FROM medikago.tindakan_poli ORDER BY id_tindakan_poli")
        data = cursor.fetchall()
        for i in data:
            data_tindakan_poli.append({
                "id_tindakan_poli" : f"{i[0]}",
                "id_poliklinik" : f"{i[1]}",
                "nama_tindakan" : f"{i[2]}",
                "tarif"         : f"{i[3]}",
                "deskripsi"     : f"{i[4]}",
            })
        response = {'view_daftar_tindakan_poli' : data_tindakan_poli}
        return render(request, 'daftar_tindakan_poli.html', response)
    return redirect(reverse("login:home"))

# belom selesai
def update_tindakan_poli(request):
    response = {}
    cursor = connection.cursor()
    
    if(request.method == "GET" and request.session["user"] == "administrator"):
        try:
            if(request.GET['id_tindakan_poli']):
                pass
        except:
            return redirect(reverse("app2:view_daftar_tindakan_poli"))

        
        id_tindakan_poli = request.GET['id_tindakan_poli']
        cursor.execute(f"SELECT * FROM medikago.tindakan_poli where id_tindakan_poli = '{id_tindakan_poli}'")
        select = cursor.fetchone()
        if(select):
            data = {
                    "id_tindakan_poli" : f"{select[0]}",
                    "id_poliklinik" : f"{select[1]}",
                    "nama_tindakan" : f"{select[2]}",
                    "tarif"         : f"{select[3]}",
                    "deskripsi"     : f"{select[4]}",
                }
        else :
            return redirect(reverse("app2:view_daftar_tindakan_poli"))

        response['data_update'] = data

        cursor.execute("SELECT id_poliklinik FROM medikago.layanan_poliklinik ORDER BY id_poliklinik")
        select2 = cursor.fetchall()
        response["id_poliklinik_choices"] = [select2[x][0] for x in range(len(select2))]
            
        return render(request, 'update_tindakan_poli.html', response)
    elif(request.method == "POST"):
        cursor = connection.cursor()
        id_tindakan_poli = request.POST["id_tindakan_poli"]
        id_poliklinik = request.POST["id_poliklinik"]
        nama_tindakan = request.POST["nama_tindakan"]
        deskripsi = request.POST["deskripsi"]
        tarif = request.POST["tarif"]
        
        try:
            cursor.execute(f"UPDATE medikago.tindakan_poli SET id_poliklinik = '{id_poliklinik}', nama_tindakan = '{nama_tindakan}', deskripsi = '{deskripsi}', tarif = {tarif} WHERE id_tindakan_poli = '{id_tindakan_poli}'")
        except:
            messages.error(request, "Update Tindakan Poliklinik Gagal Dilakukan")
            return redirect(reverse("app2:update_tindakan_poli"))
        return redirect(reverse("app2:view_daftar_tindakan_poli"))
    return redirect(reverse("login:pengguna"))

# udah 
def delete_tindakan_poli(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        try:
            if(request.GET['id_tindakan_poli']):
                id_tindakan_poli= request.GET['id_tindakan_poli']
                cursor.execute(f"DELETE FROM medikago.tindakan_poli WHERE id_tindakan_poli = '{id_tindakan_poli}'")
                return redirect(reverse("app2:view_daftar_tindakan_poli"))
        except:
            return redirect(reverse("app2:view_daftar_tindakan_poli"))
    return redirect(reverse("login:pengguna"))

### TINDAKAN ####
# belom selesai
def create_tindakan(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        create_tindakan = CreateTindakanForm()
        response = {'create_form': create_tindakan}
        return render(request, 'create_tindakan.html', response)
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        cursor.execute("SELECT no_urut FROM medikago.tindakan ORDER BY no_urut::int DESC LIMIT 1")
        no_urut = str(int(cursor.fetchone()[0]) + 1)
        
        id_konsultasi = request.POST["id_konsultasi"]
        id_transaksi = request.POST["id_transaksi"]
        catatan = request.POST["catatan"]
        id_tindakan_poli = request.POST["id_tindakan_poli"]
        
        try:
            cursor.execute(f"INSERT INTO medikago.tindakan (id_konsultasi, no_urut, biaya, catatan, id_transaksi) VALUES ('{id_konsultasi}', '{no_urut}', 0, '{catatan}', '{id_transaksi}')")
            cursor.execute(f"INSERT INTO medikago.daftar_tindakan (id_konsultasi, no_urut, id_tindakan_poli) VALUES ('{id_konsultasi}', '{no_urut}', '{id_tindakan_poli}')")
        except:
            messages.error(request, "Create Tindakan Gagal Dilakukan")
            return redirect(reverse("app2:create_tindakan"))
        return redirect(reverse("app2:view_daftar_tindakan"))
    return redirect(reverse("login:pengguna"))

# udah tp yg [5] msh ???
def view_daftar_tindakan(request):
    response = {}
    data_tindakan = []
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["user"] != "nothing"):
        cursor.execute(f"SELECT * FROM medikago.tindakan ORDER BY id_konsultasi")
        data = cursor.fetchall()
        for i in data:
            cursor.execute(f"SELECT id_tindakan_poli FROM medikago.daftar_tindakan WHERE id_konsultasi='{i[0]}' AND no_urut='{i[1]}'")
            select = cursor.fetchone()
            if(not select):
                select = " "
            data_tindakan.append({
                "id_konsultasi" : f"{i[0]}",
                "no_urut" : f"{i[1]}",
                "biaya" : f"{i[2]}",
                "catatan" : f"{i[3]}",
                "id_transaksi" : f"{i[4]}",
                "daftar_id_tindakan_poli" : f"{select[0]}",
            })
            
            
        response = {'view_daftar_tindakan' : data_tindakan}

        

        return render(request, 'daftar_tindakan.html', response)
    return redirect(reverse("login:home"))

# belom selesai
def update_tindakan(request):
    response = {}
    cursor = connection.cursor()
    
    if(request.method == "GET" and request.session["user"] == "administrator"):
        
        try:
            if(request.GET['id_konsultasi'] and request.GET['no_urut'] ):
                pass
        except:
            return redirect(reverse("app2:view_daftar_tindakan"))

        
        id_konsultasi = request.GET['id_konsultasi']
        no_urut = request.GET['no_urut']
        cursor.execute(f"SELECT * FROM medikago.tindakan where id_konsultasi = '{id_konsultasi}' AND no_urut = '{no_urut}'")
        select = cursor.fetchone()
        if(select):
            cursor.execute(f"SELECT id_tindakan_poli FROM medikago.daftar_tindakan WHERE id_konsultasi='{id_konsultasi}' AND no_urut='{no_urut}'")
            select2 = cursor.fetchone()
            if(not select2):
                select2 = " "
            data = {
                    "id_konsultasi" : f"{select[0]}",
                    "no_urut" : f"{select[1]}",
                    "biaya" : f"{select[2]}",
                    "catatan" : f"{select[3]}",
                    "id_transaksi" : f"{select[4]}",
                    "id_tindakan_poli" : f"{select2[0]}"
                }
        else :
            return redirect(reverse("app2:view_daftar_tindakan"))

        response['data_update'] = data
            
        return render(request, 'update_tindakan.html', response)
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        id_konsultasi = request.POST["id_konsultasi"]
        no_urut = request.POST["no_urut"]
        catatan = request.POST["catatan"]
        
        try:
            cursor.execute(f"UPDATE medikago.tindakan SET catatan = '{catatan}' WHERE id_konsultasi = '{id_konsultasi}' AND no_urut = '{no_urut}'")
        except:
            messages.error(request, "Update Tindakan Gagal Dilakukan")
            return redirect(reverse("app2:update_tindakan"))
        return redirect(reverse("app2:view_daftar_tindakan"))
    return redirect(reverse("login:pengguna"))

def delete_tindakan(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        try:
            if(request.GET['id_konsultasi']):
                id_konsultasi = request.GET['id_konsultasi']
                no_urut = request.GET['no_urut']
                cursor.execute(f"DELETE FROM medikago.tindakan WHERE id_konsultasi = '{id_konsultasi}' AND no_urut = '{no_urut}'")
                return redirect(reverse("app2:view_daftar_tindakan"))
        except:
            return redirect(reverse("app2:view_daftar_tindakan"))
    return redirect(reverse("login:pengguna"))