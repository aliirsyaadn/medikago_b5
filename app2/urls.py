from django.urls import path
from . import views

app_name = 'app2'

urlpatterns = [
	path('profil', views.profil, name='profil'),
	path('create_tindakan_poli', views.create_tindakan_poli, name='create_tindakan_poli'),
    path('view_daftar_tindakan_poli', views.view_daftar_tindakan_poli, name='view_daftar_tindakan_poli'),
    path('update_tindakan_poli', views.update_tindakan_poli, name='update_tindakan_poli'),
    path('delete_tindakan_poli', views.delete_tindakan_poli, name='delete_tindakan_poli'),
	path('create_tindakan', views.create_tindakan, name='create_tindakan'),
    path('view_daftar_tindakan', views.view_daftar_tindakan, name='view_daftar_tindakan'),
    path('update_tindakan', views.update_tindakan, name='update_tindakan'),
    path('delete_tindakan', views.delete_tindakan, name='delete_tindakan'),
]