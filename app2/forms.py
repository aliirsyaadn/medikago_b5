from django import forms
from django.db import connection

cursor = connection.cursor()
cursor.execute("SELECT id_poliklinik FROM medikago.layanan_poliklinik ORDER BY id_poliklinik")
select = cursor.fetchall()

id_poliklinik = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]

### TINDAKAN POLIKLINIK ###
class CreateTindakanPoliForm(forms.Form):
    id_poliklinik= forms.CharField(label="ID Poliklinik", widget=forms.Select(choices=id_poliklinik, attrs={
        'class': 'form-control',
    }))
    nama_tindakan = forms.CharField(label="Nama Tindakan", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Tindakan',
        'type' : 'text',
        'required': True,
    }))
    deskripsi = forms.CharField(label="Deskripsi", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Deskripsi',
        'type' : 'text',
        'required': True,
    }))
    tarif = forms.CharField(label="Tarif", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tarif',
        'type' : 'text',
        'required': True,
    }))

class UpdateTindakanPoliForm(forms.Form):
# yang ga disabled
    nama_tindakan = forms.CharField(label="Nama Tindakan", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Tindakan',
        'type' : 'text',
        'required': True,
    }))
    deskripsi = forms.CharField(label="Deskripsi", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Deskripsi',
        'type' : 'text',
        'required': True,
    }))
    tarif = forms.CharField(label="Tarif", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tarif',
        'type' : 'text',
    }))
            

### TINDAKAN ###
cursor = connection.cursor()
cursor.execute("SELECT id_konsultasi FROM medikago.tindakan ORDER BY id_konsultasi")
select = cursor.fetchall()
id_konsultasi = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]

cursor.execute("SELECT id_transaksi FROM medikago.tindakan ORDER BY id_transaksi")
select = cursor.fetchall()
id_transaksi = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]

cursor.execute("SELECT id_tindakan_poli FROM medikago.tindakan_poli ORDER BY id_tindakan_poli")
select = cursor.fetchall()
id_tindakan_poli = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]

class CreateTindakanForm(forms.Form):
    id_konsultasi= forms.CharField(label="ID Konsultasi", widget=forms.Select(choices=id_konsultasi, attrs={
        'class': 'form-control'
    }))
    id_transaksi= forms.CharField(label="ID Transaksi", widget=forms.Select(choices=id_transaksi, attrs={
        'class': 'form-control'
    }))
    catatan = forms.CharField(label="Catatan", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Catatan',
        'type' : 'text',
        'required': True,
    }))
    id_tindakan_poli= forms.CharField(label="Daftar ID Tindakan Poliklinik", widget=forms.Select(choices=id_tindakan_poli, attrs={
        'class': 'form-control'
    }))

class UpdateTindakanForm(forms.Form):
# yang ga disabled
    catatan = forms.CharField(label="Catatan", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Catatan',
        'type' : 'text',
        'required': True,
    }))