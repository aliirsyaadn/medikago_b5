from django import forms


class SignUpAdminForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Username',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': 'Password',
        'required': True,
    }))

    nomor_identitas = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Identitas',
        'type' : 'text',
        'required': True,
    }))

    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))


    tanggal_lahir = forms.DateField(input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
    'class': 'form-control',
    'placeholder': 'Tanggal Lahir',
    'type': 'date',
    'required': True,
    }))

    email = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
    }))

    alamat = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Alamat',
        'type' : 'text',
        'required': True,
    }))

class SignUpDokterForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Username',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': 'Password',
        'required': True,
    }))

    nomor_identitas = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Identitas',
        'type' : 'text',
        'required': True,
    }))

    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))


    tanggal_lahir = forms.DateField(input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
    'class': 'form-control',
    'placeholder': 'Tanggal Lahir',
    'type': 'date',
    'required': True,
    }))

    email = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
    }))

    alamat = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Alamat',
        'type' : 'text',
        'required': True,
    }))

    no_sip = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'No SIP',
        'type' : 'text',
        'required': True,
    }))

    spesialisasi = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Spesialisasi',
        'type' : 'text',
        'required': True,
    }))


class SignUpPasienForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Username',
        'type' : 'text',
        'required': True,
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': 'Password',
        'required': True,
    }))

    nomor_identitas = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Identitas',
        'type' : 'text',
        'required': True,
    }))

    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Lengkap',
        'type' : 'text',
        'required': True,
    }))


    tanggal_lahir = forms.DateField(input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
    'class': 'form-control',
    'placeholder': 'Tanggal Lahir',
    'type': 'date',
    'required': True,
    }))

    email = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Email',
        'type' : 'text',
        'required': True,
    }))

    alamat = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Alamat',
        'type' : 'text',
        'required': True,
    }))

    alergi = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Alergi',
        'type' : 'text',
        'required': True,
    }))
