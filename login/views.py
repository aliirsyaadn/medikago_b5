from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from .forms import *

import random

# Create your views here.
def home(request):
    try:
        if(request.session["user"]):
            pass
    except:
        request.session["user"] = "nothing"

    return render(request, 'home.html')

def signup(request):
    if(request.method == "GET" and request.session["user"] == "nothing"):
        return render(request,'signup.html')
    return redirect(reverse("login:home"))
       
    

def signup_admin(request):
    if(request.method == "GET" and request.session["user"] == "nothing"):
        signup_admin = SignUpAdminForm()
        response = {
            'admin_form': signup_admin,
        }
        return render(request, 'signup_admin.html', response)
    elif(request.method == "POST" and request.session["user"] == "nothing"):
        cursor = connection.cursor()
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nama_lengkap = request.POST['nama_lengkap']
        tanggal_lahir = request.POST['tanggal_lahir']
        email = request.POST['email']
        alamat = request.POST['alamat']
        
        # Insert to table pengguna
        try:
            cursor.execute(f"INSERT INTO medikago.pengguna (email, username, password, nama_lengkap, nomor_id, tanggal_lahir, alamat) VALUES ('{email}','{username}','{password}', '{nama_lengkap}', '{nomor_identitas}','{tanggal_lahir}', '{alamat}')")
        except:
            messages.error(request, "Data yang anda masukkan salah")
            return redirect(reverse("login:signup_admin"))
        
        # Generate random
        nomor_pegawai = f"{random.randint(0,9999)}"
        
        # Insert to table admin
        # Asumsi kode rs default RSTU
        cursor.execute(f"INSERT INTO medikago.administrator (nomor_pegawai, username, kode_rs) VALUES ('{nomor_pegawai}', '{username}', 'RSTU')")
        
        request.session["user"] = "administrator"
        request.session["id"] = nomor_pegawai

        return redirect(reverse("login:pengguna"))
    return redirect(reverse("login:home"))

def signup_dokter(request):
    if(request.method == "GET" and request.session["user"] == "nothing"): 
        signup_dokter = SignUpDokterForm()
        response = {
            'dokter_form': signup_dokter,
        }
        return render(request, 'signup_dokter.html', response)
    elif(request.method == "POST" and request.session["user"] == "nothing"):
        cursor = connection.cursor()
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nama_lengkap = request.POST['nama_lengkap']
        tanggal_lahir = request.POST['tanggal_lahir']
        email = request.POST['email']
        alamat = request.POST['alamat']
        no_sip = request.POST['no_sip']
        spesialisasi = request.POST['spesialisasi']

        # Insert to table pengguna
        try:
            cursor.execute(f"INSERT INTO medikago.pengguna (email, username, password, nama_lengkap, nomor_id, tanggal_lahir, alamat) VALUES ('{email}','{username}','{password}', '{nama_lengkap}', '{nomor_identitas}','{tanggal_lahir}', '{alamat}')")
        except:
            messages.error(request, "Data yang anda masukkan salah")
            return redirect(reverse("login:signup_dokter"))
        
        # Generate random
        id_dokter = f"{random.randint(0,9999)}"
        
        # Insert to table dokter
        cursor.execute(f"INSERT INTO medikago.dokter (id_dokter, username, no_sip, spesialisasi) VALUES ('{id_dokter}', '{username}', '{no_sip}', '{spesialisasi}')")
        
        request.session["user"] = "dokter"
        request.session["id"] = id_dokter

        return redirect(reverse("login:pengguna"))
    return redirect(reverse("login:home"))

def signup_pasien(request):
    if(request.method == "GET" and request.session["user"] == "nothing"):   
        signup_pasien = SignUpPasienForm()
        response = {
            'pasien_form': signup_pasien,
        }
        return render(request, 'signup_pasien.html', response)
    elif(request.method == "POST" and request.session["user"] == "nothing"):
        cursor = connection.cursor()
        username = request.POST['username']
        password = request.POST['password']
        nomor_identitas = request.POST['nomor_identitas']
        nama_lengkap = request.POST['nama_lengkap']
        tanggal_lahir = request.POST['tanggal_lahir']
        email = request.POST['email']
        alamat = request.POST['alamat']
        # Insert to table pengguna
        try:
            cursor.execute(f"INSERT INTO medikago.pengguna (email, username, password, nama_lengkap, nomor_id, tanggal_lahir, alamat) VALUES ('{email}','{username}','{password}', '{nama_lengkap}', '{nomor_identitas}','{tanggal_lahir}', '{alamat}')")
        except:
            messages.error(request, "Data yang anda masukkan salah")
            return redirect(reverse("login:signup_pasien"))
        
        # Generate random
        prefix1 = random.randint(100,999)
        prefix2 = random.randint(10,99)
        prefix3 = random.randint(1000,9999)
        no_rekam_medis = f"{prefix1}-{prefix2}-{prefix3}"
        
        # Insert to table pasien
        # Asumsi nama asuransi default Leannon LLC
        cursor.execute(f"INSERT INTO medikago.pasien (no_rekam_medis, username, nama_asuransi) VALUES ('{no_rekam_medis}', '{username}','Leannon LLC')")
        
        request.session["user"] = "pasien"
        request.session["id"] = no_rekam_medis

        return redirect(reverse("login:pengguna"))
    return redirect(reverse("login:home"))


def auth(request):
    if(request.method == "POST" and request.session["user"] == "nothing"):
        cursor = connection.cursor()
        username = request.POST['username']
        password = request.POST['password']
        cursor.execute(f"SELECT * FROM medikago.pengguna where username='{username}' AND password='{password}'")
        select = cursor.fetchone()
        if(select):
            request.session["username"] = username

            cursor.execute(f"SELECT * FROM medikago.administrator where username='{username}'")
            select = cursor.fetchone()
            if(select):
                # select is tuple (nomor_pegawai, username, kode_rs)
                request.session["user"] = "administrator"
                request.session["id"] = select[0]

                return redirect(reverse("login:admin"))
            
            cursor.execute(f"SELECT * FROM medikago.dokter where username='{username}'")
            select = cursor.fetchone()
            if(select):
                # select is tuple (id_dokter, username, no_sip, spesialisasi)
                request.session["user"] = "dokter"
                request.session["id"] = select[0]

                return redirect(reverse("login:pengguna"))
           
            cursor.execute(f"SELECT * FROM medikago.pasien where username='{username}'")
            select = cursor.fetchone()
            if(select):
                # select is tuple (no_rekam_medis, username, nama_asuransi)
                request.session["user"] = "pasien"
                request.session["id"] = select[0]

                return redirect(reverse("login:pengguna"))
            
            return redirect(reverse("login:home"))
            
        
        messages.error(request, "Username atau password yang anda masukkan salah")
        return redirect(reverse("login:home"))

    return redirect(reverse("login:home"))
            
def admin(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        return render(request, 'admin.html')
    return redirect(reverse("login:home"))

def pengguna(request):
    if(request.method == "GET" and (request.session["user"] == "dokter" or request.session["user"] == "pasien")):
        return render(request, 'pengguna.html')
    return redirect(reverse("login:home"))

def logout(request):
    if(request.session["user"] != "nothing"):
        messages.success(request, "Anda berhasil keluar dari sistem.")
        request.session.flush()

    return redirect(reverse("login:home"))
    
