from django.urls import path
from . import views

app_name = 'login'
urlpatterns = [
    path('', views.home, name='home'),
    path('signup/',views.signup, name='signup'),
    path('signup/admin',views.signup_admin, name='signup_admin'),
    path('signup/dokter',views.signup_dokter, name='signup_dokter'),
    path('signup/pasien',views.signup_pasien, name='signup_pasien'),
    path('auth', views.auth, name='auth'),
    path('logout', views.logout, name='logout'),
    path('admin', views.admin, name='admin'),
    path('pengguna', views.pengguna, name='pengguna'),
]