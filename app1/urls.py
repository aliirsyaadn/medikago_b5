from django.urls import path
from . import views

app_name = 'app1'
urlpatterns = [
    path('', views.create_sesikonsultasi, name='create_sesikonsultasi'),
    path('daftar_sesi_konsultasi', views.daftar_sesikonsultasi, name='daftar_sesikonsultasi'),
    path('update_sesi_konsultasi', views.update_sesikonsultasi, name='update_sesikonsultasi'),
    path('delete_sesi_konsultasi', views.delete_sesikonsultasi, name='delete_sesikonsultasi'),
    path('create_rs_cabang',views.create_rs_cabang, name='create_rs_cabang'),
    path('daftar_rs_cabang', views.daftar_rs_cabang, name='daftar_rs_cabang'),
    path('update_rs_cabang', views.update_rs_cabang, name='update_rs_cabang'),
    path('delete_rs_cabang', views.delete_rs_cabang, name= 'delete_rs_cabang'),
]