from django.shortcuts import render, redirect, reverse
from .forms import *
from django.db import connection
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import datetime


# Create your views here.
def create_sesikonsultasi(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        create_sesikonsultasi = CreateSesiKonsultasiForm()
        response = {'create_form_sesikonsultasi': create_sesikonsultasi}
        return render(request, 'create_sesikonsultasi.html', response)
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        cursor.execute("SELECT id_konsultasi FROM medikago.sesi_konsultasi ORDER BY id_konsultasi DESC LIMIT 1")
        id_konsultasi = cursor.fetchone()[0].split("-")
        # print(id_konsultasi)
        if(id_konsultasi[-1] == "9999" and id_konsultasi[-2] != "99"):
            id_konsultasi[-2] = str(int(id_konsultasi[-2])+1)
            id_konsultasi[-1] = "0000"
        elif(id_konsultasi[-1] == "9999" and id_konsultasi[-2] == "99"):
            id_konsultasi[-3] = str(int(id_konsultasi[-3])+1)
            id_konsultasi[-2] = "00"
            id_konsultasi[-1] = "0000"
        else:
            id_konsultasi[-1] = str(int(id_konsultasi[-1])+1)

        id_konsultasi = "-".join(id_konsultasi)
        tanggal = request.POST["tanggal"]
        biaya = 0
        status = "Booked"
        no_rekam_medis_pasien = request.POST["dropdown_pasien"]
        id_transaksi = request.POST["dropdown_id_transaksi"]
        try:
            cursor.execute(f"INSERT INTO medikago.sesi_konsultasi (id_konsultasi, no_rekam_medis_pasien, tanggal, biaya, status, id_transaksi) VALUES ('{id_konsultasi}','{no_rekam_medis_pasien}','{tanggal}','{biaya}','{status}','{id_transaksi}')")
            messages.info(request,"Create Sesi Konsultasi Berhasil")
        except:
            messages.error(request, "Create Sesi Konsultasi Gagal, Coba Ulangi Lagi")
            return redirect(reverse("app1:create_sesikonsultasi"))
        return redirect(reverse("app1:daftar_sesikonsultasi"))
    return redirect(reverse("login:pengguna"))

def daftar_sesikonsultasi(request):
    response = {}
    data_sesikonsultasi = []
    cursor = connection.cursor()

    if(request.method == "GET" and request.session["user"] != "nothing"):
        cursor.execute(f"SELECT * FROM medikago.sesi_konsultasi ORDER BY id_konsultasi")
        data = cursor.fetchall()
        for i in data:
            data_sesikonsultasi.append({
                "id_konsultasi" : f"{i[0]}",
                "no_pasien" : f"{i[1]}",
                "tanggal": f"{i[2]}",
                "biaya": f"{i[3]}",
                "status": f"{i[4]}",
                "id_transaksi": f"{i[5]}"
            })
        # data = cursor.fetchone()
        # print(data)
        response = {'daftar_sesi_konsultasi': data_sesikonsultasi}
        return render(request, 'daftar_sesikonsultasi.html',response)
    return redirect(reverse("login:home"))

def update_sesikonsultasi(request):
    response = {}
    cursor = connection.cursor()
    if (request.method == "GET" and request.session["user"] == "administrator"):
        update_sesi_konsultasi = UpdateSesiKonsultasiForm()
        response = {
            'update_form': update_sesi_konsultasi,
        }
        try:
            if(request.GET['id_konsultasi']):
                pass
        except:
            return redirect(reverse("app1:daftar_sesikonsultasi"))
            
        id_konsultasi = request.GET['id_konsultasi']
        cursor.execute(f"SELECT * FROM medikago.sesi_konsultasi where id_konsultasi = '{id_konsultasi}'")
        select = cursor.fetchone()
        if(select):
            data = {
                "id_konsultasi": f"{select[0]}",
                "no_pasien": f"{select[1]}",
                "tanggal": f"{select[2]}",
                "biaya" : f"{select[3]}",
                "status" : f"{select[4]}",
                "id_transaksi" :f"{select[5]}"
            } 
        else:
            return redirect(reverse("app1:daftar_sesikonsultasi"))
        
        response['data_update'] = data
        return render(request, 'update_sesikonsultasi.html', response)

    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        id_konsultasi = request.POST["id_konsultasi"]
        tanggal = request.POST["tanggal"]
        status = request.POST["status"]
        try:
            if(status == "Done"):
                cursor.execute(f"UPDATE medikago.sesi_konsultasi SET tanggal = '{tanggal}',status = '{status}' WHERE id_konsultasi = '{id_konsultasi}'")
                messages.info(request,"Update Sesi Konsultasi Berhasil")
            else:
                messages.info(request,"Update Sesi Konsultasi gagal atau belum terupdate, Coba Ulangi Lagi")
        except:
            return redirect(reverse("app1:update_sesikonsultasi"))
        return redirect(reverse("app1:daftar_sesikonsultasi"))

    return redirect(reverse("login:pengguna"))


def delete_sesikonsultasi(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        id_konsultasi = request.GET["id_konsultasi"]
        try:
            cursor.execute(f"DELETE FROM medikago.sesi_konsultasi where id_konsultasi = '{id_konsultasi}'")
            messages.info(request,"Delete Sesi Konsultasi Berhasil")
        except:
            messages.error(request,"Sesi Konsultasi Gagal di Hapus")
            return redirect(reverse("app1:daftar_sesikonsultasi"))
        return redirect(reverse("app1:daftar_sesikonsultasi"))
    return redirect(reverse("login:pengguna"))

def create_rs_cabang(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        create_rs_cabang = CreateRsCabangForm()
        response = {'create_form_rs_cabang': create_rs_cabang}
        return render(request, 'create_rs_cabang.html', response)
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        kode_rs = request.POST["kode_rs_cabang"]
        nama_rs = request.POST["nama_rs"]
        tanggal_pendirian = request.POST["tanggal_pendirian"]
        jalan = request.POST["jalan"]
        nomor = request.POST["nomor"]
        kota = request.POST["kota"]
        try:
            cursor.execute(f"INSERT INTO medikago.rs_cabang VALUES ('{kode_rs}','{nama_rs}','{tanggal_pendirian}','{jalan}','{nomor}','{kota}')")
            messages.info(request,"RS Cabang Berhasil di Tambahkan")
        except:
            messages.error(request,"Create RS Cabang gagal !! terdapat kesamaan pada Kode RS, Coba Ulangi Lagi")
            return redirect(reverse("app1:create_rs_cabang"))
        return redirect(reverse("app1:daftar_rs_cabang"))
    return redirect(reverse("login:pengguna"))

def daftar_rs_cabang(request):
    response = {}
    data_rs_cabang = []
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["user"] != "nothing"):
        cursor.execute(f"SELECT * FROM medikago.rs_cabang ORDER BY kode_rs")
        data = cursor.fetchall()
        for i in data:
            data_rs_cabang.append({
                "kode_rs" : f"{i[0]}",
                "nama_rs" : f"{i[1]}",
                "tanggal_pendirian": f"{i[2]}",
                "jalan": f"{i[3]}",
                "nomor": f"{i[4]}",
                "kota": f"{i[5]}"
            })
        response = {'daftar_rs_cabang': data_rs_cabang}
        return render(request, 'daftar_rs_cabang.html',response)
    return redirect(reverse("login:home"))

def update_rs_cabang(request):
    response = {}
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["user"] == "administrator"):
        update_rs_cabang = UpdateRsCabangForm()
        response = {
            'update_form' : update_rs_cabang,
        }
        try:
            if(request.GET['kode_rs']):
                pass
        except:
            return redirect(reverse("app1:daftar_rs_cabang"))

        kode_rs = request.GET['kode_rs']
        cursor.execute(f"SELECT * FROM medikago.rs_cabang where kode_rs = '{kode_rs}'")
        select = cursor.fetchone()
        if(select):
            data = {
                "kode_rs": f"{select[0]}",
                "nama_rs": f"{select[1]}",
                "tanggal_pendirian": f"{select[2]}",
                "jalan" : f"{select[3]}",
                "nomor" : f"{select[4]}",
                "kota"  : f"{select[5]}"
            }
            # print(data)
        else:
            return redirect(reverse("app1:daftar_rs_cabang"))

        response['data_update'] = data
        return render(request,'update_rs_cabang.html',response)
        
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        kode_rs = request.POST["kode_rs"]
        nama_rs = request.POST["nama_rs"]
        tanggal_pendirian = request.POST["tanggal_pendirian"]
        jalan = request.POST["jalan"]
        nomor = request.POST["nomor"]
        kota = request.POST["kota"]
        try:
            cursor.execute(f"UPDATE medikago.rs_cabang SET nama = '{nama_rs}',tanggal_pendirian = '{tanggal_pendirian}', jalan = '{jalan}', nomor = '{nomor}', kota ='{kota}' WHERE kode_rs = '{kode_rs}'")
            messages.info(request,"Update RS Cabang Berhasil")
        except:
            messages.error(request,"Update rs cabang gagal, coba ulangi lagi")
            return redirect(reverse("app1:update_rs_cabang"))
        return redirect(reverse("app1:daftar_rs_cabang"))
    return redirect(reverse("login:pengguna"))

def delete_rs_cabang(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        kode_rs = request.GET["kode_rs"]
        try:
            cursor.execute(f"DELETE from medikago.rs_cabang where kode_rs = '{kode_rs}'")
            messages.info(request,"RS Cabang Berhasil di Hapus")
        except:
            messages.error(request, "RS Cabang Gagal di Hapus")
            return redirect(reverse("app1:daftar_rs_cabang"))
        return redirect(reverse("app1:daftar_rs_cabang"))
    return redirect(reverse("login:pengguna"))