from django import forms
from django.db import connection

cursor = connection.cursor()
cursor.execute("SELECT no_rekam_medis FROM medikago.pasien")
select = cursor.fetchall()
cursor.execute("SELECT id_transaksi FROM medikago.transaksi")
select2 = cursor.fetchall()

no_rekam_medis_choices = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]
id_transaksi_choices = [tuple([select2[x][0],select2[x][0]]) for x in range(len(select2))]

class CreateSesiKonsultasiForm(forms.Form):
    dropdown_pasien = forms.CharField(label="Nomor Rekam Medis Pasien ", widget=forms.Select(choices=no_rekam_medis_choices, attrs={
        'class': 'form-control'
    }))
    tanggal = forms.DateField(input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
        'class': 'form-control',
        'placeholder': 'tanggal',
        'type': 'date',
        'required': True,
    }))
    dropdown_id_transaksi = forms.CharField(label="Id Transaksi",widget=forms.Select(choices=id_transaksi_choices,attrs={
        'class': 'form-control'
    }))
# Unused

class UpdateSesiKonsultasiForm(forms.Form):
    tanggal = forms.DateField(input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
        'class': 'form-control',
        'placeholder': 'tanggal',
        'type': 'date',
        'required': True,
    }))
    status = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'status',
        'type' : 'text',
        'required': True,
    }))

class CreateRsCabangForm(forms.Form):
    kode_rs_cabang = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kode Rs Cabang',
        'type' : 'text',
        'required': True,
    }))
    nama_rs = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Rs',
        'type' : 'text',
        'required': True,
    }))
    tanggal_pendirian = forms.DateField(label='Tanggal Pendirian', input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tanggal Pendirian',
        'type': 'date',
        'required': True,
    }))
    jalan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jalan',
        'type' : 'text',
        'required': True,
    }))
    nomor = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor',
        'type' : 'text',
        'required': True,
    }))
    kota = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kota',
        'type' : 'text',
        'required': True,
    }))
class UpdateRsCabangForm(forms.Form):
    nama_rs = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Rs',
        'type' : 'text',
        'required': True,
    }))
    tanggal_pendirian = forms.DateField(label='Tanggal Pendirian',input_formats=['%Y-%m-%d'],widget=forms.widgets.DateInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tanggal Pendirian',
        'type': 'date',
        'required': True,
    }))
    jalan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jalan',
        'type' : 'text',
        'required': True,
    }))
    nomor = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor',
        'type' : 'text',
        'required': True,
    }))
    kota = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kota',
        'type' : 'text',
        'required': True,
    }))