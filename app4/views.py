from django.shortcuts import render, redirect, reverse
from .forms import *
from django.db import connection
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import datetime
from collections import namedtuple


# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def create_dokter_rs_cabang(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        create_dokter_rs_cabang = CreateDaftarDokterForm()
        response = {'create_form_daftar_dokter': create_dokter_rs_cabang}
        return render(request, 'create_dokter_rs_cabang.html', response)
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        id_dokter = request.POST["id_dokter"]
        kode_rs = request.POST["kode_rs"]
        try:
            cursor.execute(f"INSERT INTO medikago.dokter_rs_cabang VALUES ('{id_dokter}','{kode_rs}')")
            messages.info(request,"Dokter RS Cabang Berhasil di Tambahkan")
        except:
            messages.error(request,"Create RS Cabang gagal !! terdapat kesamaan pada Kode RS, Coba Ulangi Lagi")
            return redirect(reverse("app4:create_dokter_rs_cabang"))
        # print(request.POST)
        return redirect(reverse("app4:daftar_dokter_rs_cabang"))
    return redirect(reverse("login:pengguna"))

def daftar_dokter_rs_cabang(request):
    response = {}
    data_dokter_rs_cabang = []
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["user"] != "nothing"):
        cursor.execute(f"SELECT kode_rs,id_dokter FROM medikago.dokter_rs_cabang ORDER BY kode_rs")
        data = cursor.fetchall()
        for i in data:
            data_dokter_rs_cabang.append({
                "id_dokter" : f"{i[1]}",
                "kode_rs" : f"{i[0]}"
            })
        response = {'daftar_dokter_rs_cabang': data_dokter_rs_cabang}
        # print(data)
        return render(request, 'daftar_dokter_rs_cabang.html',response)
    return redirect(reverse("login:home"))


def update_dokter_rs_cabang(request):
    response = {}
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["user"] == "administrator"):
        try:
            if(request.GET['kode_rs'] and request.GET['id_dokter'] ):
                pass
        except:
            return redirect(reverse("app4:daftar_dokter_rs_cabang"))

        response["kode_rs"] = request.GET['kode_rs']
        response["id_dokter"] = request.GET['id_dokter']

        cursor.execute("SELECT id_dokter FROM medikago.dokter order by id_dokter")
        select = cursor.fetchall()
        cursor.execute("SELECT kode_rs FROM medikago.rs_cabang order by kode_rs")
        select2 = cursor.fetchall()

        response["id_dokter_choices"] = [select[x][0] for x in range(len(select))]
        response["kode_rs_choices"] = [select2[x][0] for x in range(len(select2))]

        return render(request,'update_dokter_rs_cabang.html',response)
        
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        id_dokter = request.POST["id_dokter"]
        kode_rs = request.POST["kode_rs"]
        id_dokter_new = request.POST["id_dokter_new"]
        kode_rs_new = request.POST["kode_rs_new"]

        try:
            cursor.execute(f"UPDATE medikago.dokter_rs_cabang SET id_dokter = '{id_dokter_new}',kode_rs = '{kode_rs_new}' where id_dokter = '{id_dokter}' AND kode_rs = '{kode_rs}'")
            messages.info(request,"Update Dokter RS Cabang Berhasil")
        except:
            messages.error(request,"Update dokter rs cabang gagal, coba ulangi lagi")
            return redirect(reverse("app4:update_dokter_rs_cabang"))
        return redirect(reverse("app4:daftar_dokter_rs_cabang"))
    return redirect(reverse("login:pengguna"))


def delete_dokter_rs_cabang(request):
    if (request.method == 'GET' and request.session["user"] == 'administrator'):
        cursor = connection.cursor()
        try:
            if(request.GET["id_dokter"] and request.GET["kode_rs"]):
                id_dokter = request.GET["id_dokter"]
                kode_rs = request.GET["kode_rs"]
                cursor.execute(f"DELETE FROM medikago.dokter_rs_cabang WHERE kode_rs = '{kode_rs}' AND id_dokter = '{id_dokter}'")
                messages.info(request,"Delete Dokter RS Cabang Berhasil")
                return redirect(reverse('app4:update_dokter_rs_cabang'))
        except:
            messages.error(request,"Delete dokter rs cabang gagal, coba ulangi lagi")
            return redirect(reverse('app4:update_dokter_rs_cabang'))

        return redirect(reverse('app4:update_dokter_rs_cabang'))


def create_layanan_poliklinik(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        create_layanan_poliklinik = CreatePoliklinikForm()
        response = {'create_layanan_poliklinik': create_layanan_poliklinik}
        return render(request, 'create_layanan_poliklinik.html', response)
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        cursor.execute("SELECT id_poliklinik FROM medikago.layanan_poliklinik ORDER BY id_poliklinik DESC LIMIT 1")

        id_poliklinik = cursor.fetchone()[0].split("-")
        id_poliklinik[-1] = str(int(id_poliklinik[-1]) + 1)
        id_poliklinik = "-".join(id_poliklinik)

        nama = request.POST["nama_layanan"]
        deskripsi = request.POST["deskripsi"]
        kode_rs_cabang = request.POST["kode_rs_cabang"]


        cursor.execute("SELECT id_jadwal_poliklinik FROM medikago.jadwal_layanan_poliklinik ORDER BY id_jadwal_poliklinik DESC LIMIT 1")

        id_jadwal_poliklinik = cursor.fetchone()[0].split("-")
        id_jadwal_poliklinik[-1] = str(int(id_jadwal_poliklinik[-1]) + 1)
        id_jadwal_poliklinik = "-".join(id_jadwal_poliklinik)
        id_dokter = 1
        hari = request.POST["hari"]
        waktu_mulai = request.POST["mulai"]
        waktu_selesai = request.POST["selesai"]
        kapasitas = request.POST["kapasitas"]
        try:
            cursor.execute(f"INSERT INTO medikago.layanan_poliklinik VALUES ('{id_poliklinik}','{kode_rs_cabang}', '{nama}','{deskripsi}')")
            cursor.execute(f"INSERT INTO medikago.jadwal_layanan_poliklinik VALUES ('{id_jadwal_poliklinik}','{waktu_mulai}', '{waktu_selesai}','{hari}',{kapasitas},'{id_dokter}','{id_poliklinik}' )")
        except:

            return redirect(reverse("app4:create_layanan_poliklinik"))
        # print(request.POST)
        return redirect(reverse("app4:daftar_layanan_poliklinik"))
    return redirect(reverse("login:pengguna"))



def daftar_layanan_poliklinik(request):
    response = {}
    data_daftar_layanan_poli = []
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["user"] != "nothing"):
        cursor.execute(f"SELECT * FROM medikago.layanan_poliklinik ORDER BY id_poliklinik")
        data = cursor.fetchall()
        for i in data:
            data_daftar_layanan_poli.append({
                "id_poliklinik" : f"{i[0]}",
                "kode_rs_cabang" : f"{i[1]}",
                "nama" : f"{i[2]}",
                "deskripsi" : f"{i[3]}"
            })
        response['table'] = data_daftar_layanan_poli

        return render(request, 'daftar_layanan_poliklinik.html',response)
    return redirect(reverse("login:home"))




def update_layanan_poliklinik(request, id):
    response = {}
    cursor = connection.cursor()
    if(request.method == "GET" and request.session["user"] == "administrator"):
        
        cursor.execute(f"SELECT * FROM medikago.layanan_poliklinik WHERE id_poliklinik = '{id}'")
        select = cursor.fetchone()
        if(select):
            data = {
                "id_poliklinik": f"{select[0]}",
                "kode_rs_cabang": f"{select[1]}",
                "nama": f"{select[2]}",
                "deskripsi" : f"{select[3]}"
            }
        else:
            return redirect(reverse("app4:daftar_layanan_poliklinik"))
        response['data_update'] = data


        cursor.execute("SELECT kode_rs FROM medikago.rs_cabang order by kode_rs")
        select2 = cursor.fetchall()
        response["kode_rs_choices"] = [select2[x][0] for x in range(len(select2))]

        
        return render(request,'update_layanan_poliklinik.html', response)
        
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        nama = request.POST["nama_layanan"]
        deskripsi = request.POST["deskripsi"]
        kode_rs_cabang = request.POST["kode_rs_cabang"]
        try:
            cursor.execute(f"UPDATE medikago.layanan_poliklinik SET nama = '{nama}', deskripsi = '{deskripsi}', kode_rs_cabang = '{kode_rs_cabang}' WHERE id_poliklinik = '{id}'")
            # messages.info(request,"Update Layanan Poliklinik Berhasil")
        except:
            # messages.error(request,"Update layanan poliklinik gagal, coba ulangi lagi")
            return redirect(reverse("app4:update_layanan_poliklinik"))
        return redirect(reverse("app4:daftar_layanan_poliklinik"))
    return redirect(reverse("login:pengguna"))


def delete_layanan_poliklinik(request, id):
    if (request.method == 'GET' and request.session["user"] == 'administrator'):
        cursor = connection.cursor()
        delete_layanan = "delete from medikago.layanan_poliklinik where id_poliklinik = '" + id + "'"
        cursor.execute(delete_layanan)
        return redirect(reverse('app4:daftar_layanan_poliklinik'))



def daftar_jadwal_poliklinik_khusus(request, id):
    response = {}
    data_jadwal = []
    cursor = connection.cursor()
    
    if(request.method == "GET" and request.session["user"] != "nothing"):
        cursor.execute(f"SELECT * FROM medikago.jadwal_layanan_poliklinik WHERE id_poliklinik = '{id}'")
        data = cursor.fetchall()
        for i in data:
            data_jadwal.append({
                "id_jadwal_poliklinik" : f"{i[0]}",
                "hari" : f"{i[3]}",
                "waktu_mulai" : f"{i[1]}",
                "waktu_selesai" : f"{i[2]}",    
                "kapasitas" : f"{i[4]}",
                "id_dokter" : f"{i[5]}",
                "id_poliklinik" : f"{i[6]}",
            })

        response = {'jadwal' : data_jadwal}
        return render(request, 'daftar_jadwal_poliklinik.html', response)
    return redirect(reverse("login:home"))


def daftar_jadwal_poliklinik(request):
    response = {}
    data_jadwal = []
    cursor = connection.cursor()
    
    if(request.method == "GET" and request.session["user"] != "nothing"):
        cursor.execute(f"SELECT * FROM medikago.jadwal_layanan_poliklinik ORDER BY id_poliklinik")
        data = cursor.fetchall()
        for i in data:
            data_jadwal.append({
                "id_jadwal_poliklinik" : f"{i[0]}",
                "hari" : f"{i[3]}",
                "waktu_mulai" : f"{i[1]}",
                "waktu_selesai" : f"{i[2]}",    
                "kapasitas" : f"{i[4]}",
                "id_dokter" : f"{i[5]}",
                "id_poliklinik" : f"{i[6]}",
            })

        response = {'jadwal' : data_jadwal}
        return render(request, 'daftar_jadwal_poliklinik.html', response)
    return redirect(reverse("login:home"))

def update_jadwal_poliklinik(request, id):
    response = {}
    cursor = connection.cursor()

    if(request.method == "GET" and request.session["user"] == "administrator"):
        
        cursor.execute(f"SELECT * FROM medikago.jadwal_layanan_poliklinik WHERE id_jadwal_poliklinik = '{id}'")
        select = cursor.fetchone()
        if(select):
            data = {
                "id_jadwal_poliklinik": f"{select[0]}",
                "waktu_mulai": f"{select[1]}",
                "waktu_selesai": f"{select[2]}",
                "hari" : f"{select[3]}",
                "kapasitas" : f"{select[4]}",
                "id_dokter" : f"{select[5]}",
                "id_poliklinik" : f"{select[6]}"

            }
        else:
            return redirect(reverse("app4:daftar_layanan_poliklinik"))
        
        response['data_update'] = data

        cursor.execute("SELECT id_dokter FROM medikago.dokter order by id_dokter")
        select2 = cursor.fetchall()
        response["id_dokter_choices"] = [select2[x][0] for x in range(len(select2))]

        return render(request,'update_jadwal_poliklinik.html', response)
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        hari = request.POST["hari"] 
        waktu_mulai = request.POST["waktu_mulai"] 
        waktu_selesai = request.POST["waktu_selesai"] 
        kapasitas = request.POST["kapasitas"] 
        id_dokter = request.POST["id_dokter"] 

        try:
            cursor.execute(f"UPDATE medikago.jadwal_layanan_poliklinik SET hari = '{hari}', waktu_mulai = '{waktu_mulai}', waktu_selesai = '{waktu_selesai}', kapasitas = '{kapasitas}', id_dokter = '{id_dokter}' WHERE id_jadwal_poliklinik = '{id}'")
            messages.info(request,"Update Jadwal Poliklinik Berhasil")
        except:
            messages.error(request,"Update jadwal poliklinik gagal, coba ulangi lagi")
            return redirect(reverse("app4:update_jadwal_poliklinik"))
        return redirect(reverse("app4:daftar_jadwal_poliklinik"))
    return redirect(reverse("login:pengguna"))


def delete_jadwal_poliklinik(request, id):
    if (request.method == 'GET' and request.session['user'] == 'administrator'):
        cursor = connection.cursor()
        delete_jadwal = "delete from medikago.jadwal_layanan_poliklinik where id_jadwal_poliklinik = '" + id + "'"
        cursor.execute(delete_jadwal)
        return redirect(reverse('app4:daftar_jadwal_poliklinik'))



