from django.contrib import admin
from django.conf.urls import url
from django.urls import path
from .views import *
from . import views

app_name = 'app4'

urlpatterns = [
    path('create_dokter_rs_cabang', create_dokter_rs_cabang, name='create_dokter_rs_cabang'),
    path('daftar_dokter_rs_cabang', daftar_dokter_rs_cabang, name='daftar_dokter_rs_cabang'),
    path('update_dokter_rs_cabang', update_dokter_rs_cabang, name='update_dokter_rs_cabang'),
    path('delete_dokter_rs_cabang', delete_dokter_rs_cabang, name='delete_dokter_rs_cabang'),

    path('create_layanan_poliklinik', create_layanan_poliklinik, name='create_layanan_poliklinik'),
    path('daftar_layanan_poliklinik', daftar_layanan_poliklinik, name='daftar_layanan_poliklinik'),
    path('update_layanan_poliklinik/<id>/', update_layanan_poliklinik, name='update_layanan_poliklinik'),
    path('delete_layanan_poliklinik/<id>/', delete_layanan_poliklinik, name='delete_layanan_poliklinik'),

    path('daftar_jadwal_poliklinik', daftar_jadwal_poliklinik, name='daftar_jadwal_poliklinik'),
    path('daftar_jadwal_poliklinik/<id>/', daftar_jadwal_poliklinik_khusus, name='daftar_jadwal_poliklinik'),
    path('update_jadwal_poliklinik/<id>/', update_jadwal_poliklinik, name='update_jadwal_poliklinik'),
    path('delete_jadwal_poliklinik/<id>/', delete_jadwal_poliklinik, name='delete_jadwal_poliklinik'),

]