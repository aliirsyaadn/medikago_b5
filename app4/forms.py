from django import forms
from django.db import connection

cursor = connection.cursor()
cursor.execute("SELECT id_dokter FROM medikago.dokter order by id_dokter")
select = cursor.fetchall()
cursor.execute("SELECT kode_rs FROM medikago.rs_cabang order by kode_rs")
select2 = cursor.fetchall()

id_dokter_choices = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]
Kode_RS_choices = [tuple([select2[x][0],select2[x][0]]) for x in range(len(select2))]


# Tindakan Poliklinik

class CreateDaftarDokterForm(forms.Form):
    id_dokter = forms.CharField(label="ID Dokter", widget=forms.Select(choices=id_dokter_choices, attrs={
        'class': 'form-control'
    }))

    kode_rs = forms.CharField(label="Kode RS", widget=forms.Select(choices=Kode_RS_choices, attrs={
        'class': 'form-control'
    }))

class CreatePoliklinikForm(forms.Form):

    nama_layanan = forms.CharField(label='Nama Layanan', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'required': True}) )
    deskripsi = forms.CharField(label='Deskripsi', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'required': True}))
    kode_rs_cabang = forms.CharField(label="Kode RS", widget=forms.Select(choices=Kode_RS_choices, attrs={
        'class': 'form-control'
    }))
    hari = forms.CharField(label='Hari', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'required': True}))
    mulai = forms.CharField(label='Waktu Mulai', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'required': True}))
    selesai = forms.CharField(label='Waktu Selesai', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'required': True}))
    kapasitas = forms.CharField(label='Kapasitas', max_length=50, widget=forms.TextInput(attrs={'class': 'form-control', 'required': True}))


