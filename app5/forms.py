from django import forms
from django.db import connection

cursor = connection.cursor()
cursor.execute("SELECT no_rekam_medis FROM medikago.pasien")
select = cursor.fetchall()

no_rekam_medis = [tuple([select[x][0], select[x][0]]) for x in range(len(select))]

class CreateTransaksiForm(forms.Form):
    dropdown_pasien = forms.CharField(label="Nomor Rekam Medis Pasien ", widget=forms.Select(choices=no_rekam_medis, attrs={
        'class': 'form-control'
    }))

class UpdateTransaksiForm(forms.Form):
    tanggal = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Tanggal',
        'type' : 'text',
        'required': True,
    }))

    status = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Status',
        'type' : 'text',
        'required': True,
    }))

    waktu_pembayaran = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Waktu Pembayaran',
        'type' : 'text',
        'required': True,
    }))