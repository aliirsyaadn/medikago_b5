from django.shortcuts import render, redirect, reverse
from django.db import connection
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from .forms import CreateTransaksiForm, UpdateTransaksiForm
import datetime

def create_transaksi(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        create_transaksi = CreateTransaksiForm()
        response = {'create_form': create_transaksi}
        return render(request, 'create_transaksi.html', response)
    elif(request.method == "POST" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        cursor.execute("SELECT id_transaksi FROM medikago.transaksi ORDER BY id_transaksi DESC LIMIT 1")

        id_transaksi = cursor.fetchone()[0].split("-")
        id_transaksi[-1] = str(int(id_transaksi[-1]) + 1)
        id_transaksi = "-".join(id_transaksi)
        tanggal = datetime.date.today()
        status = "Created"
        total_biaya = 0
        waktu_pembayaran = datetime.datetime.now()
        no_rekam_medis = request.POST["dropdown_pasien"]  
        try:
            cursor.execute(f"INSERT INTO medikago.transaksi (id_transaksi, tanggal, status, total_biaya, waktu_pembayaran, no_rekam_medis) VALUES ('{id_transaksi}', '{tanggal}', '{status}', {total_biaya}, '{waktu_pembayaran}', '{no_rekam_medis}')")
        except:
            messages.error(request, "Create Transaksi gagal dilakukan")
            return redirect(reverse("app5:create_transaksi"))
        
        return redirect(reverse("app5:daftar_transaksi"))
    return redirect(reverse("login:pengguna"))

def daftar_transaksi(request):
    response = {}
    data_transaksi = []
    cursor = connection.cursor()

    if(request.method == "GET" and request.session["user"] != "nothing"):
        cursor.execute(f"SELECT * FROM medikago.transaksi ORDER BY id_transaksi")
        data = cursor.fetchall()
        for i in data:
            data_transaksi.append({
                "id_transaksi" : f"{i[0]}",
                "tanggal" : f"{i[1]}",
                "status" : f"{i[2]}",
                "total_biaya" : f"{i[3]}",
                "waktu_pembayaran" : f"{i[4]}",
                "no_rekam_medis" : f"{i[5]}"
            })

        response = {'daftar_transaksi' : data_transaksi}
        return render(request, 'daftar_transaksi.html', response)
    return redirect(reverse("login:home"))

def update_transaksi(request):
    response = {}
    cursor = connection.cursor()
    
    if(request.method == "GET" and request.session["user"] == "administrator"):
        update_transaksi = UpdateTransaksiForm()
        response = {
            'update_form': update_transaksi,
        }
        
        try:
            if(request.GET['id_transaksi']):
                pass
        except:
            return redirect(reverse("app5:daftar_transaksi"))

        
        id_transaksi = request.GET['id_transaksi']
        cursor.execute(f"SELECT * FROM medikago.transaksi where id_transaksi = '{id_transaksi}'")
        select = cursor.fetchone()
        if(select):
            data = {
                    "id_transaksi" : f"{select[0]}",
                    "tanggal" : f"{select[1]}",
                    "status" : f"{select[2]}",
                    "total_biaya" : f"{select[3]}",
                    "waktu_pembayaran" : f"{select[4]}",
                    "no_rekam_medis" : f"{select[5]}"
                }
        else :
            return redirect(reverse("app5:daftar_transaksi"))

        response['data_update'] = data
            
        return render(request, 'update_transaksi.html', response)
    elif(request.method == "POST"):
        cursor = connection.cursor()
        id_transaksi = request.POST["id_transaksi"]
        tanggal = request.POST["tanggal"]
        status = request.POST["status"]
        waktu_pembayaran = request.POST["waktu_pembayaran"]
        
        try:
            cursor.execute(f"UPDATE medikago.transaksi SET tanggal = '{tanggal}', status = '{status}', waktu_pembayaran = '{waktu_pembayaran}' WHERE id_transaksi = '{id_transaksi}'")
        except:
            messages.error(request, "Update Transaksi gagal dilakukan")
            return redirect(reverse("app5:update_transaksi"))

        return redirect(reverse("app5:daftar_transaksi"))

    return redirect(reverse("login:pengguna"))

def delete_transaksi(request):
    if(request.method == "GET" and request.session["user"] == "administrator"):
        cursor = connection.cursor()
        try:
            if(request.GET['id_transaksi']):
                id_transaksi = request.GET['id_transaksi']
                cursor.execute(f"DELETE FROM medikago.transaksi WHERE id_transaksi = '{id_transaksi}'")
                return redirect(reverse("app5:daftar_transaksi"))
        except:
            return redirect(reverse("app5:daftar_transaksi"))
    return redirect(reverse("login:pengguna"))