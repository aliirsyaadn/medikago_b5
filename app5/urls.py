from django.urls import path
from . import views

app_name = 'app5'
urlpatterns = [
    path('', views.create_transaksi, name='create_transaksi'),
    path('daftar_transaksi', views.daftar_transaksi, name='daftar_transaksi'),
    path('update_transaksi', views.update_transaksi, name='update_transaksi'),
    path('delete_transaksi', views.delete_transaksi, name='delete_transaksi'),
]